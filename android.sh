#!/bin/bash

# -install change install
# -s size of img
# -m memory
# -iso path to iso file
# -img path to iso file

# -start change start application
# -m memory
# -img path to iso file


while [ -n "$1" ]; do # while loop starts
    case "$1" in

    -install) choice="install";;
    
    -start) choice="start";;

    -s) 
        size="$2"
        shift
        ;;

    -m) 
        memory="$2"
        shift
        ;;

    -iso) 
        iso_path="$2"
        shift
        ;;

    -img) 
        img_path="$2"
        shift
        ;;

    *) echo "wtf bro";;
    esac
    shift
done

if [ "$choice" = "install" ];
then
    qemu-img create -f qcow2 $img_path $size
    qemu-system-x86_64 \
        -enable-kvm \
        -m $memory \
        -smp 2 \
        -cpu host \
        -device ES1370 \
        -device virtio-mouse-pci -device virtio-keyboard-pci \
        -serial mon:stdio \
        -boot menu=on \
        -net nic \
        -net user,hostfwd=tcp::5555-:22 \
        -device virtio-vga,virgl=on \
        -display gtk,gl=on \
        -hda $img_path \
        -cdrom $iso_path
fi

if [ "$choice" = "start" ];
then
    qemu-system-x86_64 \
        -enable-kvm \
        -m $memory \
        -smp 2 \
        -cpu host \
        -device ES1370 \
        -device virtio-mouse-pci -device virtio-keyboard-pci \
        -serial mon:stdio \
        -boot menu=on \
        -net nic \
        -net user,hostfwd=tcp::5555-:22 \
        -device virtio-vga,virgl=on \
        -display gtk,gl=on \
        -hda $img_path
fi